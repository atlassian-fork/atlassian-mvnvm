# Puppet module for mvnvm

This is a [Puppet](https://puppet.com/) module that manages the installation of [mvnvm](http://mvnvm.org/) for \*nix platforms.

It also has the ability to 'pre-warm' certain Maven versions, so that these versions can be downloaded in advance, rather than when used for the first time.

## Usage

In `Puppetfile`:
```puppet
mod 'atlassian-mvnvm', :git => 'https://bitbucket.org/atlassian/atlassian-mvnvm.git'
```

Example usage:

```puppet
# Install Maven
class profile::maven(
  Array[String] $versions_warmed,
  Hash $capabilities,
) {
  # As mvnvm attempts to "ensure_packages('ruby')", we need to make sure
  # that our own Package['ruby'] is parsed first
  include profile::ruby

  file { '/root/.mvnvm.properties':
    ensure => file,
    source => 'puppet:///modules/profile/mvnvm.properties',
  }
  -> class { '::mvnvm':
    versions_warmed => $versions_warmed,
  }

  ::bamboo_capabilities::generate { '/buildeng-custom/capabilities/maven.sh':
    capabilities => $capabilities,
  }
}
```

