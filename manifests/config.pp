# Configure mvnvm
class mvnvm::config {
  $mvnvm::versions_warmed.each |$version| {
    mvnvm::prewarm_maven { $version:
    }
  }
}
