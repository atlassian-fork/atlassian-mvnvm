# Install mvnvm
class mvnvm::install {
  # Require unzip and ruby to download and install mvn versiosn
  ensure_packages(['unzip', 'ruby'])

  wget::fetch { 'download mvnvm':
    source      => $mvnvm::download_url,
    destination => "${mvnvm::bin_dir}/mvn",
  }
  -> file { "${mvnvm::bin_dir}/mvn":
    mode => '0755',
  }
}
