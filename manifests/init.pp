# Provide mvnvm support
class mvnvm (
  String $version,
  Stdlib::Absolutepath $bin_dir,
  Array[String] $versions_warmed = [],
  Stdlib::Httpurl $download_url = "https://bitbucket.org/mjensen/mvnvm/raw/mvnvm-${version}/mvn"
){
  class{'mvnvm::install': }
  -> class{'mvnvm::config': }
  -> Class['mvnvm']
}
